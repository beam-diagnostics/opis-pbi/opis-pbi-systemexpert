# opis-pbi-systemowner

OPI content for Proton Beam Instrumentation to be used by System Owners on the control room workstations. In the PBI context this means that these OPIs have the PBI system leads as their primary users. Secondary users cvan include operators and PBI technical staff supporting the System Leads.   

# Workflow to add content
New content/features go through a formalised approval process where the OPI developer shows the OPI to the stakeholders for feedback of whether the OPI is "ready for release". Two stakeholders are identified here. Namely the PBI System Lead as the primary user and the operations group as a secondary user. An OPI author cannot act as a reviewer in this workflow so a colleague from the same group can review in the case the OPI author is also a user of the OPI.Elena Donegani is the main contact for the PBI System Leads and Benjamin Bolling the main contact for Accelerator Operations Group. The workflow steps to add new OPI content to the repository are as follows:  
1) PBI System Lead identifies need for OPI and finds a willing OPI author to develop the OPI.
2) OPI creates a ticket in PBITECH JIRA project with label 'CR-OPI' to gather feedback from the stakeholders on whether the OPI is "ready for release" in the ESS control rooms. It should be noted that the stakeholders are primary looking for correctness. Additional "nice-to-have" features are not a rquirement for release to control room.  
3) OPI author communicates JIRA ticket to repository admin (Thomas Fay or Joao Paulo Martins) and they check the stakeholders have "signed-off" the OPI for release and that the repository house-rules on conventions have been followed.
4) The new content is then added to the repository for inclusion in the ESS control rooms.  

# Workflow for bug-fixes  
Bux-fixes can be submitted by OPI authors directly to repository admin for review. It is encouraged that bug0fixes are communicated to the stakeholders but the formalised workflow above does not need to be followed.  

 
# Conventions 
10-Top directory is for screens that can be opened directly  
99-Shared directory is for objects that are not opened directly and instead are called by/embedded in other screens.  

Things to be avoided:
Submitting content for multiple systems in one approval workflow. This is because the approval workflow needs to be handled in a lean, low-overhead manner and having only one system expert involved in each approval facilitates this aim.  
Using this repository as a development tool. This is beacuase this repository is intended to provde a formal means for including user-approved OPIs in the control room(s).  
Tracking development history. For the same rationale as above regarding deployment rather than development intentions. Therefore content additions such as BCM_v2, BPM_old etc. are not to be included in the OPIs visible from the control room(s).
